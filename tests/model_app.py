from fastapi import FastAPI, Depends
from fastapi.testclient import TestClient
from jwtdown_fastapi import authentication
from pydantic import BaseModel


class Account(BaseModel):
    email: str
    age: int
    hashed_password: str
    hashed_password_foo: str
    password_hashed: str
    password: str


class AccountOut(BaseModel):
    email: str
    age: int


class ModelAuthenticator(authentication.Authenticator):
    get_exp_called = False
    exp_account = None

    async def get_account_data(
        self,
        username: str,
        accounts: "Accounts",
    ) -> Account:
        return accounts.get_account(username)

    def get_account_getter(
        self,
        accounts: "Accounts" = Depends(),
    ) -> "Accounts":
        return accounts

    def get_hashed_password(
        self,
        account: Account,
    ):
        return account.hashed_password

    def get_exp(self, proposed, account):
        self.get_exp_called = True
        self.exp_account = account
        return super().get_exp(proposed, account)

    def reset(self):
        self.get_exp_called = False
        self.exp_account = None


SIGNING_KEY = "secret key"
model_auth = ModelAuthenticator(SIGNING_KEY)


class Accounts:
    def get_account(self, username):
        return Account(
            email=username,
            age=30,
            hashed_password=model_auth.hash_password("password"),
            hashed_password_foo=model_auth.hash_password("password"),
            password_hashed=model_auth.hash_password("password"),
            password=model_auth.hash_password("password"),
        )


model_app = FastAPI()
model_app.include_router(model_auth.router)


@model_app.get("/protected")
async def get_protected(
    account: dict = Depends(model_auth.get_current_account_data),
):
    return account


@model_app.get("/not_protected")
async def get_protected(
    account: dict = Depends(model_auth.try_get_current_account_data),
):
    return account


model_client = TestClient(model_app)
